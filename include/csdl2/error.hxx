#pragma once

#include <stdexcept>
#include <string>


namespace sdl2 {


	/*!
	 *
	 */
	class Sdl_exception : public std::exception {
     public:
        /*!
         *
         */
		Sdl_exception();

        /*!
         *
         */
        const char *what() const noexcept override;

     private:
        std::string     message_ {};
	};

	/*!
	 *
	 */
	template <class _Type>
	void check_pointer(const _Type& ptr) {
		if (ptr == nullptr) {
			throw Sdl_exception{};
		}
	}

}
