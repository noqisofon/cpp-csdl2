#pragma once

/*
 * Nuget を使う Visual Studio においては SDL.h の場所が異なります。
 */
#if defined(USE_NUGET)
#	include <SDL.h>
#else
#	include <SDL2/SDL.h>
#endif