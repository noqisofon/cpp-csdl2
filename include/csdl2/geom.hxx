#pragma once

#include <cstdint>
#include <vector>

/*
 * Nuget を使う Visual Studio においては SDL.h の場所が異なります。
 */
#if defined(USE_NUGET)
#	include <SDL.h>
#else
#	include <SDL2/SDL.h>
#endif


namespace sdl2 {


    /*!
     *
     */
    template <class _Numeric>
    struct Basic_point;

    /*!
     *
     */
    template <class _UInteger>
    struct Basic_size;

    /*!
     *
     */
    template <class _Integer, class _UInteger>
    struct Basic_rectangle;

    /*!
     *
     */
    template <class _UInteger>
    struct Basic_rgba_color;

    using Point        = Basic_point<std::int32_t>;
    using Point_vector = std::vector<Point>;

    using Size         = Basic_size<std::uint32_t>;
    using Rect         = Basic_rectangle<std::int32_t, std::uint32_t>;

    template <class _Numeric>
    struct Basic_point {
        _Numeric              x { };
        _Numeric              y { };
    };

    constexpr bool operator ==(const Point& left, const Point& right) noexcept {
        return left.x == right.x && left.y == right.y;
    }

    constexpr bool operator !=(const Point& left, const Point& right) noexcept {
        return !( left == right );
    }

    constexpr Point& operator +=(Point& left, const Point& right) noexcept {
        left.x += right.x;
        left.y += right.y;

        return left;
    }

    constexpr Point& operator -=(Point& left, const Point& right) noexcept {
        left.x -= right.x;
        left.y -= right.y;

        return left;
    }

    constexpr Point operator + (const Point& left, const Point& right) noexcept {
        return { left.x + right.x, left.y + right.y };
    }

    constexpr Point operator - (const Point& left, const Point& right) noexcept {
        return { left.x - right.x, left.y - right.y };
    }

    constexpr Point operator * (const Point& left, const Point& right) noexcept {
        return { left.x * right.x, left.y * right.y };
    }

    template <class _UInteger>
    struct Basic_size {
        _UInteger                width {};
        _UInteger                height {};
    };

    template <class _Integer, class _UInteger>
    struct Basic_rectangle {
        _Integer                 x {};
        _Integer                 y {};
        
        _UInteger                width {};
        _UInteger                height {};
    };

    template <>
    struct Basic_rectangle<std::int32_t, std::uint32_t> {
        std::int32_t             x {};
        std::int32_t             y {};
        
        std::uint32_t            width {};
        std::uint32_t            height {};

        operator SDL_Rect() const noexcept {
            return { x, y, static_cast<std::int32_t>( width ), static_cast<std::int32_t>( height ) };
        }
    };

    /*!
     *
     */
    Rect make_rect(std::int32_t x, std::int32_t y, std::uint32_t w, std::uint32_t h) noexcept;

    /*!
     *
     */
    Rect make_rect(Point a_point, std::uint32_t w, std::uint32_t h) noexcept;

    /*!
     *
     */
    Rect make_rect(Point a_point, Size a_size) noexcept;


}
