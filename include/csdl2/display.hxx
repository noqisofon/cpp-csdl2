#pragma once

#include <cstdint>
#include <string>

#include "csdl2/geom.hxx"


namespace sdl2 {

    /*!
     *
     */
    struct Screen_properties {
        std::int32_t               width { 800 };
        std::int32_t               height { 600 };

        Point                      point {};

        std::string                title {};
    };

}
