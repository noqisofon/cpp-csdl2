#pragma once

#include <cstdint>


namespace sdl2 {


    /*!
     *
     */
    class Sdl_system {
     public:
        /*!
         *
         */
        Sdl_system(std::uint32_t = SDL_INIT_EVERYTHING);
        Sdl_system(const Sdl_system&) = delete;
        Sdl_system(const Sdl_system&&) = delete;

        /*!
         *
         */
        ~Sdl_system();

     public:
        Sdl_system& operator = (const Sdl_system&) = delete;
        Sdl_system& operator = (const Sdl_system&&) = delete;
    };


}
