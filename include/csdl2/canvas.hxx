#pragma once

#include <cstdint>
#include <string>

#include "csdl2/resource.hxx"
#include "csdl2/display.hxx"


namespace sdl2 {

	
	using Color = Basic_rgba_color<std::uint8_t>;

	template <class _UInteger>
	struct Basic_rgba_color {
		_UInteger               red;
		_UInteger               green;
		_UInteger               blue;
		_UInteger               alpha;
	};

	/*!
	 *
	 */
	constexpr Color color_red(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

	/*!
	 *
	 */
	constexpr Color color_green(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

	/*!
	 *
	 */
	constexpr Color color_blue(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

	/*!
	 *
	 */
	constexpr Color color_yellow(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

	/*!
	 *
	 */
	constexpr Color color_black(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

	/*!
	 *
	 */
	constexpr Color color_white(std::uint8_t magunitude = 255, std::uint8_t alpha = 255) noexcept;

    /*!
     *
     */
    struct Angle_radian {
        double            value {};
    };

    /*!
     *
     */
    struct Line {
        Line() = default;
        Line(Point, Angle_radian, std::int32_t) noexcept;
        Line(Point, Point) noexcept;

        Point              from {};
        Point              to   {};
    };

    /*!
     * 
     */
    struct Canvas {
        Canvas() = default;

        explicit Canvas(const Screen_properties&);

        Unique_window        window { nullptr };
        Unique_renderer      renderer { nullptr };
    };

    /*!
     * 
     */
    struct Drawable_texture {
        Shared_texture       texture { nullptr };

        std::int32_t         width {};
        std::int32_t         height {};
    };


	constexpr Color color_red(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { magunitude, 0, 0, alpha };
	}

	constexpr Color color_green(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { 0, magunitude, 0, alpha };
	}

	constexpr Color color_blue(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { 0, 0, magunitude, alpha };
	}

	constexpr Color color_yellow(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { magunitude, magunitude, 0, alpha };
	}

	constexpr Color color_black(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { 0, 0, 0, alpha };
	}

	constexpr Color color_white(std::uint8_t magunitude, std::uint8_t alpha) noexcept {
		return { magunitude, magunitude, magunitude, alpha };
	}


}
