#pragma once

#include <memory>
#include <string>

/*
 * Nuget を使う Visual Studio においては SDL.h の場所が異なります。
 */
#if defined(USE_NUGET)
#	include <SDL.h>
#else
#	include <SDL2/SDL.h>
#endif


namespace sdl2 {


    /*!
     *
     */
    struct Window_deleter {
        void operator ()(SDL_Window *) const noexcept;
    };

    /*!
     *
     */
    struct Renderer_deleter {
        void operator ()(SDL_Renderer *) const noexcept;
    };

    /*!
     *
     */
    struct Surface_deleter {
        void operator ()(SDL_Surface *) const noexcept;
    };

    /*!
     *
     */
    struct Texture_deleter {
        void operator ()(SDL_Texture *) const noexcept;
    };


    using Unique_window      = std::unique_ptr<SDL_Window, Window_deleter>;
    using Unique_renderer    = std::unique_ptr<SDL_Renderer, Renderer_deleter>;
    using Unique_surface     = std::unique_ptr<SDL_Surface, Surface_deleter>;
    using Unique_texture     = std::unique_ptr<SDL_Texture, Texture_deleter>;

    using Shared_texture     = std::shared_ptr<SDL_Texture>;

    /*!
     *
     */
    Shared_texture load_texture(const std::string&, Unique_renderer);


}
