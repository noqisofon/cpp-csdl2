#include "csdl2/geom.hxx"


namespace sdl2 {


    Rect make_rect(std::int32_t x, std::int32_t y, std::uint32_t w, std::uint32_t h) noexcept {
        return { x, y, w, h };
    }

    Rect make_rect(Point a_point, std::uint32_t w, std::uint32_t h) noexcept {
        return { a_point.x, a_point.y, w, h };
    }

    Rect make_rect(Point a_point, Size a_size) noexcept {
        return { a_point.x, a_point.y, a_size.width, a_size.height };
    }


}
