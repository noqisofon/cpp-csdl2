#include "csdl2/geom.hxx"
#include "csdl2/canvas.hxx"


namespace sdl2 {


    Line::Line(Point a_point, Angle_radian an_angle, std::int32_t length) noexcept
        : from(a_point)
    {
    }

    Line::Line(Point start, Point end) noexcept
        : from(start), to(end)
    {
    }


}
