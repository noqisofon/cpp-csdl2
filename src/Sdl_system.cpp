#include "csdl2/csdl2.hxx"
#include "csdl2/error.hxx"
#include "csdl2/sdl2_system.hxx"


namespace sdl2 {


	Sdl_system::Sdl_system(std::uint32_t init_flags) {
        if ( ::SDL_Init( init_flags ) != 0 ) {
            throw Sdl_exception {};
        }
	}

    Sdl_system::~Sdl_system() {
        ::SDL_Quit();
    }


}
