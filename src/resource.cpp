#include "csdl2/csdl2.hxx"
#include "csdl2/error.hxx"
#include "csdl2/resource.hxx"

namespace sdl2 {


    
    void Window_deleter::operator ()(SDL_Window *a_window) const noexcept {
        ::SDL_DestroyWindow( a_window );
    }

    
    void Renderer_deleter::operator ()(SDL_Renderer *a_renderer) const noexcept {
        ::SDL_DestroyRenderer( a_renderer );
    }

    void Surface_deleter::operator ()(SDL_Surface *a_surface) const noexcept {
        ::SDL_FreeSurface( a_surface );
    }

    void Texture_deleter::operator ()(SDL_Texture *a_texture) const noexcept {
        ::SDL_DestroyTexture( a_texture );
    }

    Shared_texture load_texture(const std::string& filename, Unique_renderer a_renderer) {
        auto surface = Unique_surface { ::SDL_LoadBMP( filename.c_str() ) };

        check_pointer( surface );

        auto result = Shared_texture {
            ::SDL_CreateTextureFromSurface( a_renderer.get(), surface.get() ),
            Texture_deleter {}
        };

        return result;
    }


}
