#include "csdl2/csdl2.hxx"
#include "csdl2/error.hxx"


namespace sdl2 {


	Sdl_exception::Sdl_exception()
        : message_ { ::SDL_GetError() }
    {
    }

    const char *Sdl_exception::what() const noexcept {
        return message_.c_str();
    }


}
